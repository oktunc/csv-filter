package com.company.csvfilter;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.company.dto.Transaction;
import org.supercsv.cellprocessor.FmtDate;
import org.supercsv.cellprocessor.ParseBigDecimal;
import org.supercsv.cellprocessor.ParseDate;
import org.supercsv.cellprocessor.ParseLong;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanReader;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanReader;
import org.supercsv.io.ICsvBeanWriter;

import static org.supercsv.prefs.CsvPreference.STANDARD_PREFERENCE;

public class CsvFileProcessor {

    public static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";

    public List<Transaction> read(File file) throws IOException {

        List<Transaction> results = new ArrayList<>();

        try (FileReader fileReader = new FileReader(file);
             ICsvBeanReader csvBeanReader = new CsvBeanReader(fileReader, STANDARD_PREFERENCE)) {

            readTransactions(csvBeanReader, results);
        }

        return results;
    }

    public void write(List<Transaction> transactions, File file) throws IOException {

        validateOutputFile(file);

        try (FileWriter fileReader = new FileWriter(file);
             ICsvBeanWriter csvBeanWriter = new CsvBeanWriter(fileReader, STANDARD_PREFERENCE)) {

            writeTransactions(transactions, csvBeanWriter);
        }

    }

    private void writeTransactions(List<Transaction> transactions, ICsvBeanWriter csvBeanWriter) throws IOException {
        String[] headers = getHeaders();
        CellProcessor[] cellProcessors = getOutputProcessors();

        // write the header
        String[] headersUppercase = Arrays.stream(headers)
                .map(String::toUpperCase)
                .toArray(String[]::new);

        csvBeanWriter.writeHeader(headersUppercase);

        //  write the transaction lines
        for (Transaction transaction : transactions) {
            csvBeanWriter.write(transaction, headers, cellProcessors);
        }
    }

    private void readTransactions(ICsvBeanReader csvBeanReader, List<Transaction> results) throws IOException {

        String[] headers = getHeaders();
        CellProcessor[] cellProcessors = getInputProcessors();

        //  read header line
        csvBeanReader.getHeader(true);

        //  read the transaction lines
        Transaction transaction;
        while ((transaction = csvBeanReader.read(Transaction.class, headers, cellProcessors)) != null) {
            results.add(transaction);
        }
    }


    private void validateOutputFile(File file) {
        if (file.exists()) {
            throw new IllegalArgumentException("output file already exists: " + file);

        }
    }

    private static CellProcessor[] getOutputProcessors() {

        return new CellProcessor[]{
                new NotNull(),                                      // source
                new NotNull(),                                      // id
                new FmtDate(DATE_FORMAT),                           // timestamp
                new NotNull(),                                      // type
                new NotNull(),                                      // value
                new NotNull()                                       // commission
        };
    }

    private static CellProcessor[] getInputProcessors() {

        return new CellProcessor[]{
                new NotNull(),                                      // source
                new ParseLong(),                                    // id
                new ParseDate(DATE_FORMAT),                         // timestamp
                new NotNull(),                                      // type
                new ParseBigDecimal(),                              // value
                new ParseBigDecimal()                               // commission
        };
    }

    private String[] getHeaders() {
        return new String[]{"source", "id", "timestamp", "type", "value", "commission"};
    }
}
