package com.company.csvfilter;

import java.io.File;
import java.net.URI;
import java.util.*;

import com.company.dto.Transaction;
import com.company.exception.CsvFilterException;

import static java.lang.String.format;
import static java.util.Comparator.comparing;

public class CsvFilter {

    private final CsvFileProcessor csvFileProcessor;

    public CsvFilter(CsvFileProcessor csvFileProcessor) {
        this.csvFileProcessor = csvFileProcessor;
    }

    public void filter(URI inputFile1, URI inputFile2, URI outputFile) {

        List<Transaction> transactions = readDistinctTransactions(inputFile1, inputFile2);

        Comparator<Transaction> comparator = comparing(Transaction::getTimestamp)      //  sort by timestamp
                .thenComparing(Transaction::getId);                                    //  then id encase there are multiple transactions with same timestamp

        transactions.sort(comparator);

        writeTransactions(transactions, outputFile);
    }

    private void writeTransactions(List<Transaction> transactions, URI outputFile) {
        try {
            csvFileProcessor.write(transactions, new File(outputFile));
        } catch (Exception e) {
            throw new CsvFilterException(format("error writing transactions to csv file: %s", outputFile), e);
        }
    }

    private List<Transaction> readDistinctTransactions(URI inputFile1, URI inputFile2) {
        try {
            File file1 = new File(inputFile1);
            File file2 = new File(inputFile2);

            Set<Transaction> transactions = new HashSet<>(csvFileProcessor.read(file1));
            transactions.addAll(csvFileProcessor.read(file2));

            return new ArrayList<>(transactions);
        } catch (Exception e) {
            throw new CsvFilterException(format("error reading transaction from csv files: file1:%s, file2:%s", inputFile1, inputFile2), e);
        }
    }


}
