package com.company.exception;

public class CsvFilterException extends RuntimeException {

    public CsvFilterException(String message, Throwable cause) {
        super(message, cause);
    }
}
