package com.company.csvfilter;

import java.io.File;
import java.math.BigDecimal;
import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import com.company.dto.Transaction;
import com.company.exception.CsvFilterException;
import org.hamcrest.core.IsEqual;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

// NOTE: not testing for null parameter values as they are mandatory (otherwise would have used Optional)

@RunWith(MockitoJUnitRunner.class)
public class CsvFilterTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Mock
    private CsvFileProcessor mockCsvFileProcessor;

    @InjectMocks
    private CsvFilter unit;

    URI file1;
    URI file2;
    URI outputFile;

    @Before
    public void setUp() throws Exception {
        file1 = new URI("file:///csvFile1.csv");
        file2 = new URI("file:///csvFile2.csv");
        outputFile = new URI("file:///output.csv");

    }

    @Test
    public void filter_shouldThrowCsvFilterException_whenCsvFileHandlerThrowsExceptionDuringRead() throws Exception {

        RuntimeException expectedExceptionCause = new RuntimeException("read exception here");

        expectedException.expect(CsvFilterException.class);
        expectedException.expectMessage("error reading transaction from csv files: file1:file:///csvFile1.csv, file2:file:///csvFile2.csv");
        expectedException.expectCause(IsEqual.equalTo(expectedExceptionCause));

        when(mockCsvFileProcessor.read(new File(file1))).thenThrow(expectedExceptionCause);

        unit.filter(file1, file2, outputFile);
    }

    @Test
    public void filter_shouldThrowCsvFilterException_whenCsvFileHandlerThrowsExceptionDuringWrite() throws Exception {

        RuntimeException expectedExceptionCause = new RuntimeException("write exception here");

        expectedException.expect(CsvFilterException.class);
        expectedException.expectMessage("error writing transactions to csv file: file:///output.csv");
        expectedException.expectCause(IsEqual.equalTo(expectedExceptionCause));

        when(mockCsvFileProcessor.read(new File(file1))).thenReturn(emptyList());
        when(mockCsvFileProcessor.read(new File(file2))).thenReturn(emptyList());

        doThrow(expectedExceptionCause).when(mockCsvFileProcessor).write(emptyList(), new File(outputFile));

        unit.filter(file1, file2, outputFile);
    }

    @Test
    public void filter_shouldWriteDistinctAndSortedTransactions_whenCsvFileReadsSucceed() throws Exception {


        List<Transaction> file1Transactions = asList(
                buildTransaction(1, "2017-01-12T13:24:15"),
                buildTransaction(2, "2017-01-12T13:24:09"),
                buildTransaction(3, "2017-01-12T13:24:08"),
                buildTransaction(4, "2017-01-12T13:24:07"));

        List<Transaction> file2Transactions = asList(
                buildTransaction(10, "2017-01-12T13:24:10"),    //  different id, different date
                buildTransaction(2, "2017-01-12T13:24:09"),     //  duplicate
                buildTransaction(30, "2017-01-12T13:24:08"),     // different id, same date
                buildTransaction(40, "2017-01-12T13:24:20"));    // different id, different date

        when(mockCsvFileProcessor.read(new File(file1))).thenReturn(file1Transactions);
        when(mockCsvFileProcessor.read(new File(file2))).thenReturn(file2Transactions);

        unit.filter(file1, file2, outputFile);

        List<Transaction> expectedTransactions = asList(
                buildTransaction(4, "2017-01-12T13:24:07"),
                buildTransaction(3, "2017-01-12T13:24:08"),
                buildTransaction(30, "2017-01-12T13:24:08"),
                buildTransaction(2, "2017-01-12T13:24:09"),
                buildTransaction(10, "2017-01-12T13:24:10"),
                buildTransaction(1, "2017-01-12T13:24:15"),
                buildTransaction(40, "2017-01-12T13:24:20"));

        verify(mockCsvFileProcessor).write(expectedTransactions, new File(outputFile));
    }

    private Transaction buildTransaction(int id, String date) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Transaction transaction = new Transaction();
        transaction.setSource("ABC");
        transaction.setId((long) id);
        transaction.setTimestamp(simpleDateFormat.parse(date));
        transaction.setType("TYPEA");
        transaction.setValue(new BigDecimal("1200000.00"));
        transaction.setCommission(new BigDecimal("12000.00"));
        return transaction;
    }
}