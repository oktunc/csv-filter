package com.company.csvfilter;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.company.dto.Transaction;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;
import org.supercsv.exception.SuperCsvException;

import static java.nio.file.Files.readAllLines;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CsvFileProcessorTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    private CsvFileProcessor unit = new CsvFileProcessor();


    @Before
    public void setUp() throws Exception {
        temporaryFolder.create();

    }

    @Test
    public void read_shouldThrowException_whenFileDoesNotExist() throws Exception {
        expectedException.expect(IOException.class);
        expectedException.expectMessage("no-file.txt (No such file or directory)");
        unit.read(new File("no-file.txt"));

    }

    @Test
    public void read_shouldThrowException_whenCsvFileContainsBadData() throws Exception {
        expectedException.expect(SuperCsvException.class);
        expectedException.expectMessage("'BAD_VALUE_HERE' could not be parsed as a Date");
        unit.read(new File("src/test/resources/badTransactions.csv"));

    }

    @Test
    public void read_shouldReturnExpectedTransactions_whenValidFileIsGiven() throws Exception {
        List<Transaction> actual = unit.read(new File("src/test/resources/goodTransactions.csv"));

        List<Transaction> expected = buildTransactions();

        assertEquals(expected, actual);

        //  check the fileds excluded from the Transaction.equals method
        for (int i = 0; i < 2; i++) {
            assertEquals(expected.get(i).getTimestamp(), actual.get(i).getTimestamp());
            assertEquals(expected.get(i).getValue(), actual.get(i).getValue());
            assertEquals(expected.get(i).getCommission(), actual.get(i).getCommission());
        }

    }

    @Test
    public void write_shouldThrowException_whenOutputFileNotWritable() throws Exception {
        expectedException.expect(IOException.class);
        expectedException.expectMessage("Permission denied");

        File readOnlyDir = temporaryFolder.newFolder("readOnlyDir");
        readOnlyDir.setWritable(false);

        File outputFile = new File(readOnlyDir, "output.csv");

        unit.write(new ArrayList<>(buildTransactions()), outputFile);
    }

    @Test
    public void write_shouldThrowException_whenOutputFileExists() throws Exception {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("file already exists");

        File outputFile = temporaryFolder.newFile("output.csv");

        unit.write(emptyList(), outputFile);

    }

    @Test
    public void write_shouldWriteHeaderLineOnly_whenTransactionListIsEmpty() throws Exception {
        File outputFile = new File(temporaryFolder.getRoot(), "output.csv");

        unit.write(emptyList(), outputFile);

        assertTrue(outputFile.exists());
        List<String> lines = readAllLines(outputFile.toPath());
        assertEquals(1, lines.size());
        assertEquals("SOURCE,ID,TIMESTAMP,TYPE,VALUE,COMMISSION", lines.get(0));
    }

    @Test
    public void write_shouldWriteExpectedValues_whenTransactionsListContainsData() throws Exception {
        File outputFile = new File(temporaryFolder.getRoot(), "output.csv");

        unit.write(new ArrayList<>(buildTransactions()), outputFile);

        assertTrue(outputFile.exists());
        List<String> lines = readAllLines(outputFile.toPath());
        assertEquals(3, lines.size());
        assertEquals("SOURCE,ID,TIMESTAMP,TYPE,VALUE,COMMISSION", lines.get(0));
        assertEquals("ABC,923757023,2017-01-12T13:24:13,TYPEA,1200000.00,12000.00", lines.get(1));
        assertEquals("XYZ,956808949,2017-01-12T13:28:56,TYPEB,450000.00,2000.00", lines.get(2));
    }

    private List<Transaction> buildTransactions() throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

        Transaction transaction1 = new Transaction();
        transaction1.setSource("ABC");
        transaction1.setId(923757023L);
        transaction1.setTimestamp(simpleDateFormat.parse("2017-01-12T13:24:13"));
        transaction1.setType("TYPEA");
        transaction1.setValue(new BigDecimal("1200000.00"));
        transaction1.setCommission(new BigDecimal("12000.00"));

        Transaction transaction2 = new Transaction();
        transaction2.setSource("XYZ");
        transaction2.setId(956808949L);
        transaction2.setTimestamp(simpleDateFormat.parse("2017-01-12T13:28:56"));
        transaction2.setType("TYPEB");
        transaction2.setValue(new BigDecimal("450000.00"));
        transaction2.setCommission(new BigDecimal("2000.00"));

        List<Transaction> expected = new ArrayList<>();


        return asList(transaction1, transaction2);
    }

}