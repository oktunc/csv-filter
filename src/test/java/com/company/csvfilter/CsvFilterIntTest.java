package com.company.csvfilter;

import java.io.File;
import java.net.URI;
import java.nio.file.Files;
import java.util.List;

import com.company.exception.CsvFilterException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

public class CsvFilterIntTest {


    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    private CsvFilter csvFilter;

    @Before
    public void setUp() throws Exception {
        CsvFileProcessor csvFileProcessor = new CsvFileProcessor();
        csvFilter = new CsvFilter(csvFileProcessor);
    }


    @Test
    public void filter_shouldWriteExpectedException_whenValidFilesAreGiven() throws Exception {

        URI file1 = new File("src/main/resources/input/file1.csv").toURI();
        URI file2 = new File("src/main/resources/input/file2.csv").toURI();
        File outputFile = new File(temporaryFolder.getRoot(), "output.csv");

        csvFilter.filter(file1, file2, outputFile.toURI());

        List<String> writtenLines = Files.readAllLines(outputFile.toPath());

        List<String> expected = asList(
                "SOURCE,ID,TIMESTAMP,TYPE,VALUE,COMMISSION",
                "ABC,923757023,2017-01-12T13:24:13,TYPEA,1200000.00,12000.00",
                "XYZ,956808949,2017-01-12T13:28:56,TYPEB,450000.00,2000.00",
                "LMN,748034958,2017-01-12T14:02:18,TYPEA,25500000.00,95000.00",
                "DEF,693490394,2017-01-12T15:21:14,TYPEC,10000.00,500.00",
                "STU,290359876,2017-01-12T16:10:42,TYPED,380000.00,4300.00",
                "ABC,923757023,2017-01-12T16:11:09,TYPEC,2400000.00,24000.00",
                "LMN,748034959,2017-01-12T16:28:16,TYPEA,500000.00,5000.00",
                "XYZ,290359876,2017-01-12T16:31:24,TYPED,630000.00,800.00");

        assertEquals(expected, writtenLines);
    }

    @Test(expected = CsvFilterException.class)
    public void filter_shouldThrowException_whenFilterFails() throws Exception {

        URI file1 = new File("src/main/resources/input/noFile.csv").toURI();
        URI file2 = new File("src/main/resources/input/file2.csv").toURI();
        File outputFile = new File(temporaryFolder.getRoot(), "output.csv");

        csvFilter.filter(file1, file2, outputFile.toURI());
    }

}